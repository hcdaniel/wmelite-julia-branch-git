#ifndef __WMEGalaxyAPI_H__
#define __WMEGalaxyAPI_H__

#include "BScriptable.h"
#include <GalaxyAPI.h>

	template <typename T>
	class GameContextListener : public T
	{
	public:

		GameContextListener()
		{
		}

	}; 


	class AuthListener : public GameContextListener<galaxy::api::GlobalAuthListener>
	{

		CBGame* Game;
	public:

		AuthListener(CBGame* game);

		virtual void OnAuthSuccess();

		virtual void OnAuthFailure(FailureReason reason);

		virtual void OnAuthLost();
	}; 

class WMEGalaxyAPI : public CBScriptable  
{

	char* clientId;
	char* clientSecret;
	galaxy::api::IUser* galaxyUser;

	std::vector<galaxy::api::IGalaxyListener*> listeners;

	void CreateListeners();

public:
	DECLARE_PERSISTENT(WMEGalaxyAPI, CBScriptable)

	WMEGalaxyAPI(CBGame* inGame);
	WMEGalaxyAPI(CBGame* inGame, CScStack* Stack);
	virtual ~WMEGalaxyAPI(void);

	HRESULT ScCallMethod(CScScript* Script, CScStack *Stack, CScStack *ThisStack, char *Name);
	CScValue* ScGetProperty(char *Name);
	HRESULT ScSetProperty(char *Name, CScValue *Value);
	bool SetAchievement(const char* id);
	CScValue* m_Values;
	char* ScToString();

	bool m_LoggedIn;
	int m_AppId;
	int m_Length;

	bool m_GalaxyInitialized;
	bool m_StatsInitialized;
	

};

#endif