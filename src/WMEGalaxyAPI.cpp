//////////////////////////////////////////////////////////////////////////////
//// Steam Support highly inspired by steam plugin by Jan 'Mnemonic' Nedoma
///////////////////////////////////////////////////////////////////////////////


#include "dcgf.h"
#include "WMEGalaxyAPI.h"
#include <GalaxyAPI.h>


IMPLEMENT_PERSISTENT(WMEGalaxyAPI, false);

WMEGalaxyAPI::WMEGalaxyAPI(CBGame* inGame, CScStack* Stack):CBScriptable(inGame)
{
	Stack->CorrectParams(0);

	m_AppId = 0;
	m_Length = 0;
	m_Values = new CScValue(Game);

/*	bool result = SteamAPI_Init();
	m_LoggedIn = result;
	if (m_LoggedIn)
	{
		m_AppId = SteamUtils()->GetAppID();
	}
*/
}


WMEGalaxyAPI::WMEGalaxyAPI(CBGame* inGame):CBScriptable(inGame)
{
	m_AppId = 0;
	m_Length = 0;
	m_Values = new CScValue(Game);
/*
	bool result = SteamAPI_Init();
	m_LoggedIn = result;
	if (m_LoggedIn)
	{
		m_AppId = SteamUtils()->GetAppID();
	}*/
}


WMEGalaxyAPI::~WMEGalaxyAPI(void)
{
	/* SteamAPI_Shutdown();*/

	galaxy::api::Shutdown();


	SAFE_DELETE(m_Values);

}


bool WMEGalaxyAPI::SetAchievement(const char* id)
{
	const galaxy::api::IError* errCode = galaxy::api::GetError();

	if (!m_GalaxyInitialized) return false;
	if (galaxyUser != NULL)
	{
		
		galaxy::api::GalaxyID gid = galaxyUser->GetGalaxyID();
		
		errCode = galaxy::api::GetError();
		const char* txt;
			if (errCode != NULL)
			{
					txt = errCode->GetMsg();
					Game->LOG(0, "Error setting achievement: %s", txt);
			}
	
		if (galaxyUser->IsLoggedOn())
		{

			galaxy::api::Stats()->SetAchievement(id);
			errCode = galaxy::api::GetError();
			
			if (errCode != NULL)
			{
					txt = errCode->GetMsg();
					Game->LOG(0, "Error setting achievement: %s", txt);
			}
			galaxy::api::Stats()->StoreStatsAndAchievements();
			errCode = galaxy::api::GetError();

			if (errCode != NULL)
			{
					txt = errCode->GetMsg();
					Game->LOG(0, "Error storing achievement: %s", txt);
			}

			return true;
		}
	}

	return false;


	// if (!m_LoggedIn) return false;

	// if (m_StatsInitialized) return false;
	// Game->LOG(0,id);
/*	if (!SteamUser())
	{
		bool result = SteamAPI_Init();
		if (!result)
		{
			Game->LOG(0,"We don't have a steam user.");
			return false;
		}
	}
	
	if (!SteamUserStats()->SetAchievement(id)) return false;

//	Game->LOG(0,"And we even set that");
	
	return SteamUserStats()->StoreStats();*/
}





void WMEGalaxyAPI::CreateListeners()
{
		listeners.push_back(new AuthListener(Game));
/*		listeners.push_back(new OperationalStateChangeListener());
		listeners.push_back(new UserStatsAndAchievementsRetrieveListener());
		listeners.push_back(new StatsAndAchievementsStoreListener());
		listeners.push_back(new AchievementChangeListener());*/
}

HRESULT WMEGalaxyAPI::ScCallMethod(CScScript* Script, CScStack *Stack, CScStack *ThisStack, char *Name)
{
	if(strcmp(Name, "SetAchievement")==0){
		Stack->CorrectParams(1);

		char* achievement = Stack->Pop()->GetString();
		bool result = this->SetAchievement(achievement);
		Stack->PushBool(result);

		return S_OK;
	}

	if(strcmp(Name, "InitGalaxy")==0){
		Stack->CorrectParams(2);

		clientId = Stack->Pop()->GetString();
		clientSecret = Stack->Pop()->GetString();
		
		galaxy::api::Init(clientId,clientSecret,false);
		galaxy::api::ProcessData();
		const galaxy::api::IError* errCode = galaxy::api::GetError();
		m_GalaxyInitialized = true;

		const char* txt;

		if (errCode != NULL)
		{
			txt = errCode->GetMsg();
			m_GalaxyInitialized = false;
			Game->LOG(0, "Galaxy initialization error: %s", txt);
			Stack->PushBool(false);

		}
		else
		{
			Game->LOG(0, "Galaxy initialized.");
			galaxyUser = galaxy::api::User();

			CreateListeners();

			if (galaxyUser != NULL)
			{
				galaxyUser->SignIn(); //"jan.kavan@cbe-software.com","Fklpkl++123");
				errCode = galaxy::api::GetError();

				if (errCode != NULL)
				{
					txt = errCode->GetMsg();
					Game->LOG(0, "Galaxy user can't sign on: %s", txt);
					Stack->PushBool(false);
				}
				else
				{
					Stack->PushBool(true);
				}
			}
			else
			{
				Game->LOG(0, "Galaxy user object unavailable.");
				Stack->PushBool(false);
			}
		}


		return S_OK;
	}

		
	return S_OK;
}

CScValue* WMEGalaxyAPI::ScGetProperty(char *Name)
{
	m_ScValue->SetNULL();
	
	if(strcmp(Name, "Type")==0){
		m_ScValue->SetString("Galaxy");
		return m_ScValue;
	}

	if(strcmp(Name, "IsLogged")==0){
		m_ScValue->SetBool(m_LoggedIn);
		return m_ScValue;
	}


	return m_ScValue;
}

HRESULT WMEGalaxyAPI::ScSetProperty(char *Name, CScValue *Value)
{
			return S_OK;
}


HRESULT WMEGalaxyAPI::Persist(CBPersistMgr* PersistMgr)
{
	CBScriptable::Persist(PersistMgr);

	PersistMgr->Transfer(TMEMBER(m_Values));

	return S_OK;
}


char* WMEGalaxyAPI::ScToString()
{
	static char Dummy[32768];
	strcpy(Dummy, "");
	char PropName[20];
	for(int i=0; i<m_Length; i++){
		sprintf(PropName, "%d", i);
		CScValue* val = m_Values->GetProp(PropName);
		if(val){
			if(strlen(Dummy) + strlen(val->GetString()) < 32768){
				strcat(Dummy, val->GetString());
			}
		}

		if(i<m_Length-1 && strlen(Dummy)+1 < 32768) strcat(Dummy, ",");
	}
	return Dummy;
}




	AuthListener::AuthListener(CBGame* game)
		: GameContextListener()
	{
		Game = game;
	}

	void AuthListener::OnAuthSuccess()
	{
		Game->LOG(0, "GOG Galaxy:  successfully logged.");
		galaxy::api::Stats()->RequestUserStatsAndAchievements();
	}

	void AuthListener::OnAuthFailure(FailureReason reason)
	{
		Game->LOG(0, "GOG Galaxy: authorization failed with code %d",reason );
	}

	void AuthListener::OnAuthLost()
	{
		Game->LOG(0, "Galaxy says authlost.");
	} 